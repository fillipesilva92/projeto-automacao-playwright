Feature('login');

Scenario('Login com sucesso',  ({ I }) => {

  I.amOnPage('http://automationpratice.com.br/') 
  I.click('Login')
  I.waitForText('Login',10)
  I.fillField('#user','fillipe@teste.com.br')
  I.fillField('#password','123456')
  I.click('#btnLogin')
  I.waitForText('Login realizado',3)

}).tag('@sucesso')

Scenario('Tentando logar somente digitando email',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#user', 'fillipe@teste.com.br')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.',3)
  
  }).tag('@somenteemail')

  Scenario('Tentando logar sem digitar email e senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',3)
  
  }).tag('@dadosembreanco')

  Scenario('Tentando logar digitando apenas senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#password','123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',3)
  
  }).tag('somentesenha')